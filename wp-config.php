<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wordpress');

/** MySQL database password */
define('DB_PASSWORD', '123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'l+O%sYwm0r;nY&Q0&smlTfsDO|-AzS.hT4,IJhOT>NhAcWLpl<;+[%;R^{p+|mH2');
define('SECURE_AUTH_KEY',  '7e%v$X&91(Nra8f(lZ3t5-{vvG$3#%{MHdU6X3|U}UuGc6zBIhjmi!sfE=[I$=W#');
define('LOGGED_IN_KEY',    '1_vQ4<!srp|n!M-DK__|pujS(CQ(-1PzB3wsjQtr*A82xE#;s%n)ZqpPc2L5}@,)');
define('NONCE_KEY',        'r?Ov&`cT[tW9bOYzvO+!0ey)/hg2TiK W.zh[=,UadOJrqnbB#mPQHH&y)AK@i|4');
define('AUTH_SALT',        '3=}8ZQwdoiLfO$I}u]WH>;j.8nHn;i].azP.h^Yk%P&x+n*49Ho[~|#|WQ7g(|^Y');
define('SECURE_AUTH_SALT', 'q*|i|14B8aS|3LCI`myIUnLS/Su2sahG7RNn~TO$0lz/c-SW^vt_)3&);}hfMH9~');
define('LOGGED_IN_SALT',   '[4$K=q}~{g<HO1Va6jPCt#<2<?v`D< 0SixgB07r ]_><P-?Nt+=<3?e{#E]4!J^');
define('NONCE_SALT',       'd?C7:k6S[-@DOE5w-BY#$K)rAdGG?H6rrLjjrt ;XEP)TN11pYmx1T h<G@VUm9F');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');